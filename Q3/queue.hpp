//
//  queue.hpp
//  Q3
//
//  Created by kian behzad on 12/7/1396 AP.
//  Copyright © 1396 AP kian behzad. All rights reserved.
//

#ifndef queue_hpp
#define queue_hpp

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <sstream>

class Queue
{
public:
    Queue(std::string filename);
    void displayQueue();
    void enQueue(float num);
    float deQueue();
    
    
private:
    int writer;
    int reader;
    size_t freeSpace;
    bool gotthefile;
    size_t queueSize;
    float* queue;

    
    bool isFloat( std::string myString );
    
    
    
    
    
};

#endif /* queue_hpp */
