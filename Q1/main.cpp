//
//  main.cpp
//  Q1
//
//  Created by kian behzad on 12/5/1396 AP.
//  Copyright © 1396 AP kian behzad. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {

	std::cout << "//////////////////Section 1//////////////////" << std::endl;
    int a1{10};
    int* const b1{&a1};
    (*b1)++;
    a1++;
    std::cout << a1 << " " << b1 << " " << *b1;
   	std::cout << std::endl <<"/////////////////////////////////////////////" << std::endl << std::endl << std::endl;

	std::cout << "//////////////////Section 2//////////////////" << std::endl;    
    const int a2{10};
    int c2{20};
    int d2{30};
    const int* b2{&a2};
    b2 = &c2;
    std::cout << a2 << ",   " << b2 << ",   " << *b2 << std::endl;
    int* const e2{&c2};
    //e = &d;
    std::cout << d2 << ",   " << e2 << ",   " << *e2 << std::endl;
    std::cout  <<"/////////////////////////////////////////////" << std::endl << std::endl << std::endl;

    
    std::cout << "//////////////////Section 3//////////////////" << std::endl;
	char a3{'a'};
    const char* name{"Amir Jahanshahi"};
    const char* p1{name}; // ?
    std::cout << *p1 << *(p1 + 1) << *(p1 + 2) << std::endl;
    p1 = &a3; //Allowed?
    std::cout << *p1 << *(p1 + 1) << *(p1 + 2) << std::endl;
    p1 = name;
	//p1 = 'b'; //Allowed?
    //char* p2{name}; //Allowed?
    std::cout  <<"/////////////////////////////////////////////" << std::endl << std::endl << std::endl;
    
    std::cout << "//////////////////Section 4//////////////////" << std::endl;
	int* _p1{new int[10]}; //?
    int* _p2[10]; //?
    int (*_p3)(int[]); //?
    int (*_p4[10])(int [][10]); //?
    int (*_p5)[10]{new int [10][10]}; //?
    std::cout <<"/////////////////////////////////////////////" << std::endl << std::endl << std::endl;
    
    return 0;
}
